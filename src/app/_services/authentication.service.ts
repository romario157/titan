﻿
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { User } from '../_models';
@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {

        let usuario = this.http.post<any>('http://localhost:8080/login', { email: username, senha: password },{responseType: 'text' as 'json' })
            .map(user => {

               // console.log(JSON.stringify(user))
                //user.json() as User;
                if(user.headers.has("Authorization"))
                {
                    return  user.headers.get("Authorization");
            
                 //  this.setToken(token);
                }


               return null;
            });

        if (usuario) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(usuario));
        }

        return usuario;

    }
    


    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}

