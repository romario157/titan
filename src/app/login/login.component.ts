﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../_models';
import { NgForm } from '@angular/forms';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    erroLogin =false;

    constructor(private http: HttpClient,private cookieService: CookieService,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    loginUser(f: NgForm){ 

        //let creds = JSON.stringify(loginUser);
        let contentHeader = new HttpHeaders({ "Content-Type":"application/json" });
        this.http.post('http://localhost:8080/login', { email: this.model.username, senha: this.model.password }, {'headers' : new HttpHeaders ({'Content-Type' : 'application/json'}), 'responseType': 'text', observe:'response'})
         .subscribe(
         (resp?) => {
             console.log("TOKEN:  "+resp.headers.get('authorization'));
             let usuario  = new User();
               usuario.username=this.model.username;
               usuario.password=this.model.password;
               this.cookieService.set('usuario', JSON.stringify( usuario));
                this.router.navigateByUrl('/dashboard');

            // console.log("body:  "+JSON.stringify(resp.body))
             //callback(resp)
         },
         (resp) => {
           
            this.erroLogin=true;
            this.loading = false;
            f.resetForm();
         }
       );
   };
}
