﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { CookieService } from 'ngx-cookie-service';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];

    constructor(private userService: UserService,private cookieService: CookieService) {

       
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }                                                                       

    ngOnInit() {
        this.currentUser=JSON.parse(this.cookieService.get('usuario'));
        console.log(this.currentUser.username);
       // this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }
}